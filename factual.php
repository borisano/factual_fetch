<?php
  require_once('factual-php-driver/Factual.php');

  require_once('writing_to_csv.php');
  require_once('post_codes.php');
  require_once('logger.php');

  define('FACTUAL_OAUTH_KEY', 'kcTBl6ATcGXNs29Bp9mEUJzRMyw8XXyNbuyL8Twd');
  define('FACTUAL_OAUTH_SECRET', 'Cu8vGxLVr0yfMzPdow7M60FMnBYqgiFhm8JGNmrn');
  define('FACTUAL_ROW_LIMIT', 50);

  define('FACTUAL_MAX_REQUESTS_PER_DAY', 10000);
  define('MAX_REQUESTS_PER_DAY', FACTUAL_MAX_REQUESTS_PER_DAY - 600);

  define('FACTUAL_HOTEL_CATEGORY_ID', 436);
  define('FACTUAL_RESTAURANTS_CATEGORY_ID', 347);

  $factual = new Factual(FACTUAL_OAUTH_KEY,FACTUAL_OAUTH_SECRET);
  $total_requests_count = 0;

  $output_file_name = 'output/output.csv';

  $post_codes = new PostCodes();

  while($total_requests_count < MAX_REQUESTS_PER_DAY) {
    ulog('Getting next postal code');
    $post_code = $post_codes->get_next_post_code();
    if( is_null($post_code) ) {
      ulog('All post codes are fetched');
      ulog('Exiting...');
      return;
    }
    ulog("New postal code is $post_code");

    try {
      ulog("Opening $output_file_name file ...");
      $output_file = fopen($output_file_name, 'a+');

      $data = array();
      $offset = 0;
      do {
        $total_requests_count++;

        $query = new FactualQuery;
        $query->field("category_ids")->in(
          array(
            FACTUAL_HOTEL_CATEGORY_ID,
            FACTUAL_RESTAURANTS_CATEGORY_ID
          )
        );
        $query->field('postcode')->equal($post_code);
        $query->offset($offset);
        $query->limit(FACTUAL_ROW_LIMIT);

        try {
          ulog("Fetching entries from $offset to " . ($offset + FACTUAL_ROW_LIMIT) );
          $res = $factual->fetch("places", $query);
          $data = array_merge($data, $res->getData());
        } catch (FactualApiException $e) {
          ulog("ERROR! Exceeding allowed size for $post_code");
          $post_codes->mark_as_impossible($post_code);
          break; // Go for next post code
        }

        $offset += FACTUAL_ROW_LIMIT;
      } while ( count($res->getData()) == FACTUAL_ROW_LIMIT);

      $data_count = count($data);
      ulog("$data_count rows successfully fetched");

      ulog("Writing data to $output_file_name ...");
      write_to_csv($output_file, $data);
      $post_codes->mark_as_read($post_code);

    } catch (Exception $e) {
      ulog("ERROR!!!");
      ulog($e);
    } finally {
      ulog('Closing file...');
      fclose($output_file);
    }
  }

  ulog("Finishing...");
  ulog("Totally $total_requests_count requests were made");