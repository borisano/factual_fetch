# Overview
This is a script for fetching Factual API data and writing it to CSV file

## Constants
There are a couple of constants that should be described:
#### Number of records per request
 `define('FACTUAL_ROW_LIMIT', 50);` - Factual API limits number of records to be fetched in single request, by 50, so this is default value. Increasing it won't result in higher fetch rate, by may cause problems with Factual API itself.
#### Number of requests per day
```
  define('FACTUAL_MAX_REQUESTS_PER_DAY', 10000);
  define('MAX_REQUESTS_PER_DAY', FACTUAL_MAX_REQUESTS_PER_DAY - 600);
```
  Factual restricts number of requests per day to 10000. However, their documentation states that numerous exceeding this limit may result in blocking account. So I am restricting number of actual requests to lower value. Value `600` is completely arbitrary, feel free to play with it.

#### Categiry IDs
```
  define('FACTUAL_HOTEL_CATEGORY_ID', 436);
  define('FACTUAL_RESTAURANTS_CATEGORY_ID', 347);
```
Those are IDs of Factual categories script will fetch data for. If you'd like to add some categories, go to factual.php:45 and modify an array passed to `in()` function.

# Input files
## Post codes
Script expects `input/post_codes` file to be present and to contain post codes of areas to fetch in the structure of single code per line, like so:
```
10001
10002
10003
...and so on
```

## Last processed post code
Script is autmatically tracking processed post codes. Last processed post code is always written in `input/last_processed_post_code` file.
This allows script to continue fetching data for new post codes after shutting down.
This file is created automatically.

## "Impossible" post code and Factual API limits
Factual don't want us to download their entire database, so they limit max number of records per request to `50` and max offset to `500`. So the last entry we are able to rich for every particular request is `50 + 500= 550`.
If there are more data then that, script still normally write output for first 550 entries and also write current post code to `output/impossible_post_codes` in the same structure as described in `post codes` section.`

## Output
Scripts write output to `output/output.csv` file.
Structure of every row is determined in `format_entry` function of `formatting.php` file.
Current structure looks like following

`factual_id, name, postcode, address, website, email`

But this is subject to change. It supports every standard Factual field, so this set can be expanded, if needed.