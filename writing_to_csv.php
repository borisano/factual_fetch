<?php
  require_once('formatting.php');
  require_once('logger.php');

  function write_to_csv($fp, $data) {
    $rows_written_count = 0;
    foreach ($data as $entry) {
      fputcsv($fp, format_entry($entry) );
      $rows_written_count++;
    }

    ulog("$rows_written_count rows successfully written to csv");
  }