<?php
  function format_entry($entry) {
    $formatted_entry = array(
      format_field($entry,'factual_id'),
      format_field($entry,'name'),
      format_field($entry,'postcode'),
      format_address($entry),
      format_field($entry,'website'),
      format_field($entry,'email')
    );

    return $formatted_entry;
  }

  function format_field($entry, $field_name) {
    if(array_key_exists($field_name,$entry)) {
      return $entry[$field_name];
    }
    return '';
  }

  function format_address($entry) {

    return format_field($entry, 'address') .
           format_field($entry, 'address_extended');
  }