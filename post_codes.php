<?php
  class PostCodes {
    const POST_CODES_FILE_NAME = 'input/post_codes';
    const LAST_PROCESSED_POST_CODE = 'input/last_processed_post_code';
    const IMPOSSIBLE_TO_PROCESS_POST_CODES = 'output/impossible_post_codes';

    public $post_codes = array();
    public $current_post_code_index = 0;

    function __construct() {
      $this->post_codes = $this->read_post_codes();

      // Get last read post code
      try {
        $handle = fopen(self::LAST_PROCESSED_POST_CODE, 'w+');
        $last_read_post_code = fgets($handle);
        $this->current_post_code_index = (int) array_search(
          $last_read_post_code,
          $this->post_codes
        );
        $this->current_post_code_index++; // We want start querying from next value
      } catch (Exception $e) {
        $this->current_post_code_index = 0;
      }
    }

    public function get_next_post_code() {
      if(!array_key_exists($this->current_post_code_index, $this->post_codes)) {
        return null;
      }

      $new_post_code = $this->post_codes[
        $this->current_post_code_index
      ];

      $this->current_post_code_index++;

      return $new_post_code;
    }

    public function mark_as_read($code) {
      $code_index = array_search(
        $code,
        $this->post_codes
      );

      if($code_index === false) {
        ulog("ERROR! Couldn't find $code in available post codes");
        return false;
      }

      ulog("Marking post code $code as read");
      $handle = fopen(self::LAST_PROCESSED_POST_CODE, 'w');
      try {
        $fwrite = fwrite($handle, $code);
      } catch (Exception $e) {
        throw new Exception('Error saving processed post code');
      } finally {
        fclose($handle);
      }
    }

    // Specify $code as the post code with more then 550 entries
    // So not every entry for the code has been fetched
    public function mark_as_impossible($code) {
      $code_index = array_search(
        $code,
        $this->post_codes
      );

      if($code_index === false) {
        ulog("ERROR! Couldn't find $code in available post codes");
        return false;
      }

      ulog("Marking post code $code as impossible");
      $handle = fopen(self::IMPOSSIBLE_TO_PROCESS_POST_CODES, 'a');
      try {
        $fwrite = fwrite($handle, ($code . "\n") );
      } catch (Exception $e) {
        ulog("ERROR while marking post code $code as impossible");
      } finally {
        fclose($handle);
      }
    }

    private function read_post_codes() {
      ulog('Reading post codes...');
      $readed_post_codes = array();

      $codes = array();
      try {
        $handle = fopen(self::POST_CODES_FILE_NAME, 'r');
        while (!feof($handle) ) {
          $code = fgetcsv($handle);
          $codes[] = $code[0];
        }
      } catch (Exception $e) {
        ulog("ERROR Reading post codes file!");
      } finally {
        fclose($handle);
      }

      return $codes;
    }
  }